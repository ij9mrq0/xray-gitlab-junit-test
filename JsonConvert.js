var test_plan_key = process.argv[2];
console.log(test_plan_key);

var accelq_test_run_id = process.argv[3];

var jira_project_key = process.argv[4];

var fs = require('fs')

fs.readFile('result_test.txt', 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }

    var obj = JSON.parse(data);
    var NoTCs = obj.summary.testCaseSummaryList['length'];
    console.log(NoTCs);

    var src = {
        "info": {
            "project": jira_project_key,
            "summary": "Execution of automated tests for AccelQ JobId - "+accelq_test_run_id,
            "description": "This execution is automatically created",

            "user": "ci48ejp@unfi.com",
            "startDate": "2014-08-30T11:47:35+01:00",
            "finishDate": "2014-08-30T11:53:00+01:00",
            "testPlanKey": test_plan_key
        },
        "tests": [
        ]
    }

    function convert(stat) {
        if (stat == 'pass') {
            stat = "PASSED";
        }
        else if (stat == 'fail') {
            stat = "FAILED";
        }
        return stat;

    };

    for (var i = 0; i < NoTCs; i++) {
        var newRecord = {
            "testKey": obj.summary.testCaseSummaryList[i].name,
            "start": "2022-08-30T11:47:35+01:00",
            "finish": "2022-08-30T11:50:56+01:00",
            "comment": "Successful execution",
            "status": convert(obj.summary.testCaseSummaryList[i].status)
        }
        src['tests'].push(newRecord);
    }

    xray_Input = JSON.stringify(src);

    fs.writeFile("xRayInput.txt", xray_Input, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
});
