var resultId;
(() => {
    "use strict";
    var t = {
            366: function(t, e, o) {
                var n = this && this.__awaiter || function(t, e, o, n) {
                        return new(o || (o = Promise))((function(r, s) {
                            function a(t) {
                                try {
                                    l(n.next(t))
                                } catch (e) {
                                    s(e)
                                }
                            }

                            function i(t) {
                                try {
                                    l(n.throw(t))
                                } catch (e) {
                                    s(e)
                                }
                            }

                            function l(t) {
                                var e;
                                t.done ? r(t.value) : (e = t.value, e instanceof o ? e : new o((function(t) {
                                    t(e)
                                }))).then(a, i)
                            }
                            l((n = n.apply(t, e || [])).next())
                        }))
                    },
                    r = this && this.__importDefault || function(t) {
                        return t && t.__esModule ? t : {
                            default: t
                        }
                    };
                Object.defineProperty(e, "__esModule", {
                    value: !0
                });
                const s = r(o(955)),
                    a = r(o(807)),
                    i = r(o(584)),
                    l = o(906),
                    u = o(647);
                process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
                
                function d(t) {
                    return new Promise((e => setTimeout(e, t)))
                }(function() {
                    var t;
                    return n(this, void 0, void 0, (function*() {
                        try {
                            const e = new u.Command;
                            e.usage("[OPTIONS]").requiredOption("-url, --url <appURL>", "ACCELQ App URL").requiredOption("-userID, --userID <userID>", "ACCELQ User ID").requiredOption("-apiKey, --apiKey <apiKey>", "API Key").requiredOption("-t, --tenantCode <tenantCode>", "Tenant Code").requiredOption("-jobID, --jobID <jobID>", "ACCELQ CI Job ID").option("-r, --runParams [runParams]", "Run Parameters (optional)").option("-ph, --proxyHost [proxyHost]", "Proxy Host (optional)").option("-pp, --proxyPort [proxyPort]", "Proxy Port (optional)").option("-a, --agentName [agentName]", "Agent (optional)").parse(process.argv);
                            const o = e.opts().url || "",
                                r = e.opts().userID || "",
                                p = e.opts().apiKey || "",
                                f = e.opts().tenantCode || "",
                                T = e.opts().jobID || "",
                                _ = e.opts().runParams || "",
                                h = e.opts().proxyHost || "",
                                E = e.opts().proxyPort || "",
                                g = e.opts().agentName || void 0;
                            let P;
                            P = a.default.validateAppURL(o), null != P && c("ACCELQ App URL: " + P), P = a.default.validateUserId(r), null != P && c("ACCELQ User ID: " + P), P = a.default.validateAPIKey(p), null != P && c("API Key: " + P), P = a.default.validateTenantCode(f), null != P && c("Tenant Code: " + P), P = a.default.validateJobID(T), null != P && c("ACCELQ CI Job ID: " + P), P = yield function(t, e, o, r, s, a, u, c) {
                                return n(this, void 0, void 0, (function*() {
                                    i.default.setBaseURL(t, r), u && u.length > 0 ? i.default.setProxy(u, +c) : i.default.setProxy("", 0);
                                    const n = l.AQUtil.getRunParamJsonPayload(a);
                                    return yield i.default.testConnection(o, e, s, n)
                                }))
                            }(o, r, p, f, T, _, h, E), null === P ? c("Something went wrong in extension") : P && c(P), P = yield function(t, e, o, r, a, u, c, p, f) {
                                return n(this, void 0, void 0, (function*() {
                                    let n = null,
                                        T = 0;
                                    try {
                                        i.default.setBaseURL(t, r), c && c.length > 0 ? i.default.setProxy(c, +p) : i.default.setProxy("", 0);
                                        const _ = l.AQUtil.getRunParamJsonPayload(u),
                                            h = yield i.default.triggerJob(o, e, a, _, f);
                                        if (null == h) throw new Error("Unable to submit the Job, check plugin log stack");
                                        if (null != h.cause) throw new Error(h.cause);
                                        T = h.pid;
                                        let E = 0,
                                            g = 0,
                                            P = 0,
                                            S = 0,
                                            A = "",
                                            R = 0;
                                        const O = i.default.getResultExternalAccessURL(T.toString(), r);
                                        for (;;) {
                                            if (n = yield i.default.getJobSummary(T, o, e), null != n.cause) throw new Error(n.cause);
                                            if (null != n.summary && (n = n.summary), E = +n.pass, g = +n.fail, S = +n.notRun, 0 == R) {
                                                const t = n.purpose,
                                                    e = n.scnName,
                                                    o = n.testSuiteName,
                                                    r = n.testcaseCount; 
                                            }
                                            if (A = n.status.toUpperCase(), A == s.default.TEST_JOB_STATUS.COMPLETED.toUpperCase()) {
                                                const t = " " + l.AQUtil.getFormattedTime(n.startTimestamp, n.completedTimestamp);
                                            };
                                            if (P = E + g + S, A == s.default.TEST_JOB_STATUS.SCHEDULED.toUpperCase() && ++R, R == s.default.JOB_PICKUP_RETRY_COUNT) throw new Error("No agent available to pickup the job");
                                            if (A == s.default.TEST_JOB_STATUS.COMPLETED.toUpperCase() || A == s.default.TEST_JOB_STATUS.ABORTED.toUpperCase() || A == s.default.TEST_JOB_STATUS.FAILED.toUpperCase() || A == s.default.TEST_JOB_STATUS.ERROR.toUpperCase()) break;
                                            yield d(s.default.JOB_STATUS_POLL_TIME)
                                        }
                                        if (console.log(resultId/*Add result ID "Results Link: " + O*/), g > 0 || A == s.default.TEST_JOB_STATUS.ABORTED.toUpperCase() || A == s.default.TEST_JOB_STATUS.FAILED.toUpperCase() || A == s.default.TEST_JOB_STATUS.ERROR.toUpperCase()) throw new Error("Run Failed");
                                        return {
                                            status: !0
                                        }
                                    } catch (_) {
                                        if (n = yield i.default.getJobSummary(T, o, e), null != n.cause) throw new Error(n.cause);
                                        return null != n.summary && (n = n.summary), {
                                            status: !1,
                                            error: _
                                        }
                                    }
                                }))
                            }(o, r, p, f, T, _, h, E, g ? [g] : void 0), P.status
                        } catch (e) {
                            throw new Error(e.message)
                        }
                    }))
                })().then((() => process.exit(0))).catch((() => process.exit(1)))
            },
            955: (t, e) => {
                var o, n;
                Object.defineProperty(e, "__esModule", {
                        value: !0
                    }),
                    function(t) {
                        t.PASS = "pass", t.FAIL = "fail", t.NOT_RUN = "notRun", t.RUNNING = "running", t.INFO = "info", t.FATAL = "fatal", t.WARN = "warn", t.ALL = "all"
                    }(o || (o = {})),
                    function(t) {
                        t.NOT_APPLICABLE = "Not Applicable", t.SCHEDULED = "Scheduled", t.IN_PROGRESS = "In Progress", t.COMPLETED = "Completed", t.ABORTED = "Aborted", t.FAILED = "Failed To Start", t.RECURRING = "Recurring", t.ERROR = "Error", t.CONTINUOUS_INTEGRATION = "Continuous Integration"
                    }(n || (n = {}));
                const r = {
                    LOG_DELIMITER: ">>> ",
                    USER_AGENT: "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
                    JOB_STATUS_POLL_TIME: 3e4,
                    JOB_PICKUP_RETRY_COUNT: 30,
                    JOB_WEB_LINK: "#/forward?entityType=9&resultId={0}",
                    EXT_JOB_WEB_LINK: "#/resultext?tenant={0}&resultId={1}",
                    API_VERSION: "1.0",
                    AQ_RESULT_INFO_KEY: "AQReportInfo",
                    TEST_CASE_STATUS: o,
                    TEST_JOB_STATUS: n
                };
                e.default = r
            },
            807: (t, e) => {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                });
                const o = {
                    validateJobID: t => {
                        try {
                            if (+t <= 0) return "Must be a number greater than 0"
                        } catch (e) {
                            return "Not a number"
                        }
                        return null
                    },
                    validateTenantCode: function(t) {
                        return this.validateGenericField(t)
                    },
                    validateAppURL: t => {
                        try {
                            new URL(t)
                        } catch (e) {
                            return "Not a URL"
                        }
                        return null
                    },
                    validateGenericField: t => {
                        try {
                            if (null == t || 0 == t.length) return "Cannot be empty"
                        } catch (e) {
                            return "Cannot be empty"
                        }
                        return null
                    },
                    validateProjectCode: function(t) {
                        return this.validateGenericField(t)
                    },
                    validateAPIKey: function(t) {
                        return this.validateGenericField(t)
                    },
                    validateUserId: t => {
                        try {
                            const e = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
                            if (null == t || 0 == t.length) return "Cannot be empty";
                            if (!e.test(t)) return "User ID must be in email format"
                        } catch (e) {}
                        return null
                    }
                };
                e.default = o
            },
            584: function(t, e, o) {
                var n = this && this.__awaiter || function(t, e, o, n) {
                        return new(o || (o = Promise))((function(r, s) {
                            function a(t) {
                                try {
                                    l(n.next(t))
                                } catch (e) {
                                    s(e)
                                }
                            }

                            function i(t) {
                                try {
                                    l(n.throw(t))
                                } catch (e) {
                                    s(e)
                                }
                            }

                            function l(t) {
                                var e;
                                t.done ? r(t.value) : (e = t.value, e instanceof o ? e : new o((function(t) {
                                    t(e)
                                }))).then(a, i)
                            }
                            l((n = n.apply(t, e || [])).next())
                        }))
                    },
                    r = this && this.__importDefault || function(t) {
                        return t && t.__esModule ? t : {
                            default: t
                        }
                    };
                Object.defineProperty(e, "__esModule", {
                    value: !0
                });
                const s = r(o(955)),
                    a = r(o(949)),
                    i = r(o(376)),
                    l = o(906),
                    u = {
                        BASE_URL: "",
                        API_ENDPOINT: "",
                        PROXY_HOST: "",
                        PROXY_PORT: 80,
                        setProxy: function(t, e) {
                            this.PROXY_HOST = t, this.PROXY_PORT = 0 == e ? 80 : e
                        },
                        isProxySet: function() {
                            return this.PROXY_HOST && this.PROXY_HOST.length > 0
                        },
                        getProxyConfig: function() {
                            if (this.PROXY_HOST.startsWith("http")) return new a.default(`${this.PROXY_HOST}:${this.PROXY_PORT}`);
                            const t = this.BASE_URL.split("://")[0];
                            return new a.default(`${t}://${this.PROXY_HOST}:${this.PROXY_PORT}`)
                        },
                        testConnection: function(t, e, o, r) {
                            return n(this, void 0, void 0, (function*() {
                                const n = l.AQUtil.getRunParam(o, r);
                                try {
                                    let r;
                                    this.isProxySet() && (r = this.getProxyConfig());
                                    const a = yield i.default.request({
                                        method: "POST",
                                        url: `${this.API_ENDPOINT}/jobs/${o}/validate-ci-job`,
                                        httpsAgent: r,
                                        headers: {
                                            "User-Agent": s.default.USER_AGENT,
                                            API_KEY: t,
                                            USER_ID: e,
                                            "Content-Type": "application/json"
                                        },
                                        data: JSON.stringify(n)
                                    });
                                    return 200 == a.status || a.status, ""
                                } catch (a) {
                                    const t = a.response;
                                    return t && 404 != t.status ? 401 == t.status ? "Connection request failed. Please check connection parameters." : t.status >= 500 ? "Server Error: " + t.data.message : 200 != t.status ? "Template Job ID does not exist." : null : "Connection request failed. Please check the URL and Tenant Code."
                                }
                            }))
                        },
                        triggerJob: function(t, e, o, r, a) {
                            return n(this, void 0, void 0, (function*() {
                                const n = l.AQUtil.getRunParam(o, r, a);
                                try {
                                    let r;
                                    this.isProxySet() && (r = this.getProxyConfig());
                                    const a = yield i.default.request({
                                        method: "PUT",
                                        url: `${this.API_ENDPOINT}/jobs/${o}/trigger-ci-job`,
                                        httpsAgent: r,
                                        headers: {
                                            "User-Agent": s.default.USER_AGENT,
                                            API_KEY: t,
                                            USER_ID: e,
                                            "Content-Type": "application/json"
                                        },
                                        data: JSON.stringify(n)
                                    }), l = a.data;
                                    return 200 == a.status || 204 == a.status ? {
                                        pid: l[0]
                                    } : l
                                } catch (u) {
                                    return console.log("Error is: " + u.response.data.message), null
                                }
                            }))
                        },
                        setBaseURL: function(t, e) {
                            this.BASE_URL = "/" == t.charAt(t.length - 1) ? t : t + "/", this.API_ENDPOINT = this.BASE_URL + "awb/api/" + s.default.API_VERSION + "/" + e
                        },
                        getResultExternalAccessURL: function(t, e) {
                            let o = s.default.EXT_JOB_WEB_LINK;
                            resultId=t;
                            return o = o.replace("{0}", e), o = o.replace("{1}", t), this.BASE_URL + o
                        },
                        getJobSummary: function(t, e, o) {
                            return n(this, void 0, void 0, (function*() {
                                let n;
                                this.isProxySet() && (n = this.getProxyConfig());
                                return (yield i.default.request({
                                    method: "GET",
                                    httpsAgent: n,
                                    url: `${this.API_ENDPOINT}/runs/${t}`,
                                    headers: {
                                        "User-Agent": s.default.USER_AGENT,
                                        API_KEY: e,
                                        USER_ID: o,
                                        "Content-Type": "application/json"
                                    }
                                })).data
                            }))
                        }
                    };
                e.default = u
            },
            906: (t, e) => {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.AQUtil = void 0, e.AQUtil = {
                    getRunParam: (t, e, o) => {
                        let n = {};
                        return e && e.length > 0 && (n.runProperties = JSON.parse(e)), n.jobPid = +t, o && (n.agentNameList = o), n
                    },
                    getRunParamJsonPayload: t => {
                        if (null != t && 0 != t.trim().length) try {
                            return JSON.parse(t), t
                        } catch (e) {
                            const o = t.split("&");
                            let n = {};
                            return o.forEach((t => {
                                const e = t.split("=");
                                if (2 == e.length) {
                                    const t = e[0].trim(),
                                        o = e[1].trim();
                                    "" !== t && "" !== o && (n[t] = o)
                                }
                            })), JSON.stringify(n)
                        }
                    },
                    getFormattedTime: (t, e) => {
                        const o = new Date(t),
                            n = new Date(e).getTime() - o.getTime(),
                            r = +(n / 1e3 % 60).toFixed(),
                            s = +(n / 6e4 % 60).toFixed(),
                            a = +(n / 36e5 % 24).toFixed(),
                            i = +(n / 864e5 % 365).toFixed();
                        let l = "";
                        return 0 != i && (l += i > 1 ? i + " days" : i + " day"), 0 != a && (l += a > 1 ? a + " hrs" : a + " hr"), 0 != s && (l += " " + (s > 1 ? s + " mins" : s + " min")), 0 != r && (l += " " + (r > 1 ? r + " seconds" : r + " second")), l
                    }
                }
            },
            376: t => {
                t.exports = require("axios")
            },
            647: t => {
                t.exports = require("commander")
            },
            949: t => {
                t.exports = require("https-proxy-agent")
            }
        },
        e = {};
    (function o(n) {
        var r = e[n];
        if (void 0 !== r) return r.exports;
        var s = e[n] = {
            exports: {}
        };
        return t[n].call(s.exports, s, s.exports, o), s.exports
    })(366)
})();